<?php

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

$config = require __DIR__ . '/../config/web.php';
function pr($text=null, $die=false){
    #if(!defined("DEBUG")) return ;
    #if(DEBUG !== 1) return ;

    echo '<pre style="text-align: left">';
    if(is_bool($text)) {
        echo $text?'TRUE':'FALSE';
    } else {
        print_r($text);
    }
    echo '</pre>';
    if($die){
        die();
    }
}

(new yii\web\Application($config))->run();

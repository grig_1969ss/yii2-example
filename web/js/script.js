var AJAX_URL;
$( document ).ready(function() {
        if($('#brand').val()){
                getCarModels($('#brand'));
        }
});


function getCarModels(e) {
        var target = $(e).data("target");
        var brand_id = $(e).val();

        if(brand_id){
                $.ajax({
                        url: AJAX_URL,
                        type: "POST",
                        data: {
                                action:   'getCarModels',
                                brand_id:brand_id
                        },
                        success: function (data) {
                                if(data.data){
                                        $(target).html(data.data);
                                        $(target).val($('#model_val').val());
                                }else{
                                        alert('nooo');
                                }
                        },
                        error: function (error) {
                                alert('error; ' + eval(error));
                        }
                });
        }else{
                $(target).html('');
        }
}
function getFilterDatasEngine(e) {
        var target = $(e).data("target");

        var brand_elem = $(e).data("brand");
        var brand_id = $(brand_elem).val();

        var model_elem = $(e).data("car_model");
        var model_id = $(model_elem).val();

        var drive_unit_elem = $(e).data("drive_unit");
        var drive_unit = $(drive_unit_elem).val();

        var engine_type = $(e).val();

        if(engine_type){
                $.ajax({
                        url: AJAX_URL,
                        type: "POST",
                        data: {
                                action:   'getFilterDatas',
                                brand_id:brand_id,
                                model_id:model_id,
                                drive_unit:drive_unit,
                                engine_type:engine_type
                        },
                        success: function (data) {
                                if(data.data){
                                        $(target).html(data.data);
                                }else{
                                        $(target).html('');
                                }
                        },
                        error: function (error) {
                                alert('error; ' + eval(error));
                        }
                });
        }else{
                $(target).html('');
        }
}
function getFilterDatasDriveUnit(e) {
        var target = $(e).data("target");

        var brand_elem = $(e).data("brand");
        var brand_id = $(brand_elem).val();

        var model_elem = $(e).data("car_model");
        var model_id = $(model_elem).val();

        var engine_type_elem = $(e).data("engine_type");
        var engine_type = $(engine_type_elem).val();

        var drive_unit = $(e).val();

        if(drive_unit){
                $.ajax({
                        url: AJAX_URL,
                        type: "POST",
                        data: {
                                action:   'getFilterDatas',
                                brand_id:brand_id,
                                model_id:model_id,
                                drive_unit:drive_unit,
                                engine_type:engine_type
                        },
                        success: function (data) {
                                if(data.data){
                                        $(target).html(data.data);
                                }else{
                                        $(target).html('');
                                }
                        },
                        error: function (error) {
                                alert('error; ' + eval(error));
                        }
                });
        }else{
                $(target).html('');
        }
}




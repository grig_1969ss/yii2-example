<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Common
 *
 * @author Rafayel Khachatryan
 */

namespace app\common\helpers;

use yii\helpers\ArrayHelper;

class CarHelper
{

    const  ENGINE_PETROL = 1;
    const  ENGINE_DIESEL = 2;
    const  ENGINE_HYBRID = 3;

    const  DRIVE_UNIT_FULL = 1;
    const  DRIVE_UNIT_FRONT = 2;


    const  ENGINE_PETROL_TEXT = 'Бензин';
    const  ENGINE_DIESEL_TEXT = 'Дизель';
    const  ENGINE_HYBRID_TEXT = 'Гибрид';

    const  DRIVE_UNIT_FULL_TEXT = 'Полный';
    const  DRIVE_UNIT_FRONT_TEXT = 'Передний';

    public static function getEngineTypeDropdown(){
        return [
            self::ENGINE_PETROL  => self::ENGINE_PETROL_TEXT,
            self::ENGINE_DIESEL  => self::ENGINE_DIESEL_TEXT,
            self::ENGINE_HYBRID  => self::ENGINE_HYBRID_TEXT,
        ];
    }

    public static function getEngineTypeDisplayValue($key){
        return ArrayHelper::getValue(self::getEngineTypeDropdown(),$key);
    }

    public static function getDriveUnitDropdown(){
        return [
            self::DRIVE_UNIT_FULL  => self::DRIVE_UNIT_FULL_TEXT,
            self::DRIVE_UNIT_FRONT  => self::DRIVE_UNIT_FRONT_TEXT,
        ];
    }
    public static function getDriveUnitDisplayValue($key){
        return ArrayHelper::getValue(self::getDriveUnitDropdown(),$key);
    }

}

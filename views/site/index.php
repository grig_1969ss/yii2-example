<?php

/* @var $this yii\web\View */

use app\common\helpers\CarHelper;
use app\modules\admin\models\CarBrand;
use \yii\bootstrap\Html;


?>
<div class="container">
    <div class="row">
        <h1>Продажа новых автомобилей в Санкт-Петербурге <?= Yii::$app->request->get('brand')?$dataProvider->models[0]->brand->name:''?> <?= Yii::$app->request->get('model')?$dataProvider->models[0]->model->name:''?></h1>
    </div>
    <div class="row">
        <div class="col-md-4">

            <?= Html::beginForm('','get',['target'=>'_blank']) ?>
                <div class="form-group">
                    <label for="brand">Марка</label>
                    <?= Html::dropDownList('brand',Yii::$app->request->get('brand'),CarBrand::getCarBrandDropdown(),
                        [
                            'id' => 'brand',
                            'class'=>'form-control',
                            'prompt'=>'Марка',
                            'data-target'=>'#car_model',
                            'onchange'=>'getCarModels(this),this.form.submit()'
                        ])?>
                </div>
                <div class="form-group">
                    <label for="model">Модель</label>
                    <?= Html::dropDownList('model','',[],['class'=>'form-control','prompt'=>'Модель','id' => 'car_model','onchange'=>'this.form.submit()'])?>
                </div>
                <div class="form-group">
                    <label for="engine_type">Типы двигателя</label>
                    <?= Html::dropDownList('engine_type',Yii::$app->request->get('engine_type'),CarHelper::getEngineTypeDropdown(),
                        [
                            'class'=>'form-control',
                            'id'=>'engine_type',
                            'prompt'=>'Типы двигателя',
                            'data-target'=>'#site_data',
                            'data-brand'=>'#brand',
                            'data-car_model'=>'#car_model',
                            'data-drive_unit'=>'#drive_unit',
                            'onchange'=>'getFilterDatasEngine(this)'
                        ]
                    )?>
                </div>
                <div class="form-group">
                    <label for="drive_unit">Типы привода</label>
                    <?= Html::dropDownList('drive_unit',Yii::$app->request->get('drive_unit'),CarHelper::getDriveUnitDropdown(),
                        [
                            'class'=>'form-control',
                            'id'=>'drive_unit',
                            'prompt'=>'Типы привода',
                            'data-target'=>'#site_data',
                            'data-brand'=>'#brand',
                            'data-car_model'=>'#car_model',
                            'data-engine_type'=>'#engine_type',
                            'onchange'=>'getFilterDatasDriveUnit(this)'
                        ]
                    )?>
                </div>
            <?= Html::endForm() ?>
        </div>
        <div class="col-md-8" id="site_data">
            <?php foreach ($dataProvider->models as $model):?>
                <div class="col-md-4 text-center margin-md">
                    <?= Html::img('@web/images/default_car.jpg', ['alt'=>'some', 'class'=>'thing', 'width'=>'200px']);?>
                    <h3><?= $model->brand->name?></h3>
                    <h4><?= $model->model->name?></h4>
                    <h5><?= CarHelper::getEngineTypeDisplayValue($model->engine_type)?></h5>
                    <h6><?= CarHelper::getDriveUnitDisplayValue($model->drive_unit)?></h6>
                    <p><?= $model->description?></p>
                </div>

            <?php endforeach;?>
        </div>
    </div>
</div>
<?= Html::hiddenInput('', Yii::$app->request->get('model'), ['id' => 'model_val'])?>
<?php
/**
 * Created by PhpStorm.
 * User: Aram
 * Date: 1/23/2018
 * Time: 4:18 PM
 */

namespace app\controllers;

use app\common\helpers\CarHelper;
use app\modules\admin\models\CarModel;
use app\modules\admin\models\Post;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\Response;

class AjaxController extends Controller
{

    public $enableCsrfValidation=false;

    public function beforeAction($action)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return parent::beforeAction($action);
    }

    public function actionCall(){
        $params = \Yii::$app->request->post();

        $action = ArrayHelper::getValue($params,'action');
        try{
            if(empty($action)){
                throw new Exception('No action');
            }
            $data = $this->$action($params);
            return [
                'respcode'=>1,
                'respmess'=>'Ok',
                'data'=> $data
            ];
        }catch (\Exception $e){
            return [
                'respcode'=>0,
                'respmess'=>$e->getMessage(),
				'data'=>$e->getTraceAsString()
            ];
        }
    }

    public function getCarModels($params){
        $brand_id = $params['brand_id'];

        $options = CarModel::getCarModelDropdown($brand_id);

        $html = '<option value="">Модель</option>';
        foreach ($options as $value=>$label){
            $html .='<option value="'.$value.'">'.$label.'</option>';
        }
        return $html;
    }
public function getFilterDatas($params){
        $engine_type = $params['engine_type'];
        $brand_id = $params['brand_id'];
        $model_id = $params['model_id'];
        $drive_unit = $params['drive_unit'];

        $query = Post::find();

        if (!empty($engine_type)) {
            $query = $query->where(['engine_type' => $engine_type]);
        }

        if (!empty($brand_id)) {
            $query = $query->andWhere(['brand_id' => $brand_id]);
        }

        if (!empty($model_id)) {
            $query = $query->andWhere(['model_id' => $model_id]);
        }

        if (!empty($drive_unit)) {
            $query = $query->andWhere(['drive_unit' => $drive_unit]);
        }

        $query->orderBy(['id' => SORT_DESC]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => array('pageSize' => 20),
        ]);

        $html = '';
        foreach ($dataProvider->models as $model){
        $html .= '<div class="col-md-4 text-center margin-md">';
        $html .= Html::img('@web/images/default_car.jpg', ['alt' => 'some', 'class' => 'thing', 'width' => '200px']);
        $html.= '<h3>'.$model->brand->name.'</h3>';
        $html.= '<h4>'.$model->model->name.'</h4>';
        $html.= '<h5>'.CarHelper::getEngineTypeDisplayValue($model->engine_type).'</h5>';
        $html.= '<h6>'.CarHelper::getDriveUnitDisplayValue($model->drive_unit).'</h6>';
        $html.= '<p>'.$model->description.'</p>';
        $html.='</div>';

        }

        return $html;
}

}
<?php

namespace app\controllers;

use app\models\Partner;
use app\modules\admin\models\Post;
use app\modules\admin\models\Posts;
use app\modules\admin\models\search\PostsSearch;
use app\modules\admin\Module;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $query = Post::find();

        if(!empty(Yii::$app->request->get('brand'))){
            $query = $query->where(['brand_id'=>Yii::$app->request->get('brand')]);
        }

        if(!empty(Yii::$app->request->get('model'))){
            $query = $query->andWhere(['model_id'=>Yii::$app->request->get('model')]);
        }

        $query->orderBy(['id' => SORT_DESC]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => array('pageSize' => 20),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }


}

<?php

namespace app\modules\admin\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "car_model".
 *
 * @property int $id
 * @property string $name Модель
 * @property int $brand_id Марка
 * @property string $created_at Создано на
 * @property string $updated_at Редактировано на
 *
 * @property CarBrand $brand
 * @property Posts[] $posts
 */
class CarModel extends AdminModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car_model';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'brand_id'], 'required'],
            [['brand_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['brand_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarBrand::className(), 'targetAttribute' => ['brand_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Модель',
            'brand_id' => 'Марка',
            'created_at' => 'Создано на',
            'updated_at' => 'Редактировано на',
        ];
    }

    /**
     * Gets query for [[Brand]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(CarBrand::className(), ['id' => 'brand_id']);
    }

    /**
     * Gets query for [[Posts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Posts::className(), ['model_id' => 'id']);
    }

    public static function getCarModelAllDropdown(){
        return ArrayHelper::map(self::find()->all(),'id', 'name');
    }
    public static function getCarModelDropdown($brand_id){
        return ArrayHelper::map(self::find()->where(['brand_id'=>$brand_id])->all(),'id', 'name');
    }

}

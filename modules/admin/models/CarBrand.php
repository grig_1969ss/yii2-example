<?php

namespace app\modules\admin\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "car_brand".
 *
 * @property int $id
 * @property string $name Марка
 * @property string $created_at Создано на
 * @property string $updated_at Редактировано на
 *
 * @property CarModel[] $carModels
 * @property Posts[] $posts
 */
class CarBrand extends AdminModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car_brand';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'unique'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Марка',
            'created_at' => 'Создано на',
            'updated_at' => 'Редактировано на',
        ];
    }

    /**
     * Gets query for [[CarModels]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCarModels()
    {
        return $this->hasMany(CarModel::className(), ['brand_id' => 'id']);
    }

    /**
     * Gets query for [[Posts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Posts::className(), ['brand_id' => 'id']);
    }

    public static function getCarBrandDropdown(){
        return ArrayHelper::map(self::find()->all(),'id', 'name');
    }

}

<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "posts".
 *
 * @property int $id
 * @property int $brand_id Марка
 * @property int $model_id Модель
 * @property int|null $drive_unit Привод
 * @property int|null $engine_type Тип двигателя
 * @property string|null $description Описание
 * @property string|null $created_at Создано на
 * @property string|null $updated_at Редактировано на
 *
 * @property CarBrand $brand
 * @property CarModel $model
 */
class Post extends AdminModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'posts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['brand_id', 'model_id'], 'required'],
            [['brand_id', 'model_id', 'drive_unit', 'engine_type'], 'integer'],
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['brand_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarBrand::className(), 'targetAttribute' => ['brand_id' => 'id']],
            [['model_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarModel::className(), 'targetAttribute' => ['model_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'brand_id' => 'Марка',
            'model_id' => 'Модель',
            'drive_unit' => 'Привод',
            'engine_type' => 'Тип двигателя',
            'description' => 'Описание',
            'created_at' => 'Создано на',
            'updated_at' => 'Редактировано на',
        ];
    }

    /**
     * Gets query for [[Brand]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(CarBrand::className(), ['id' => 'brand_id']);
    }

    /**
     * Gets query for [[Model]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(CarModel::className(), ['id' => 'model_id']);
    }
}

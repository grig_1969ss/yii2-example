<?php


namespace app\modules\admin\models;


class AdminModel extends \yii\db\ActiveRecord
{
    public $layout='main';
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) { // new record only, otherwise time is inserted every time this record is updated
            $this->created_at = date('Y-m-d H:i:s');
        }else{
            $this->updated_at = date('Y-m-d H:i:s');
        }
        return parent::beforeSave($insert);
    }

}
<?php

use app\common\helpers\CarHelper;
use app\modules\admin\models\CarBrand;
use app\modules\admin\models\CarModel;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Post */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="post-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'brand_id')->widget(Select2::className(),  [
        'data' => CarBrand::getCarBrandDropdown(),
        'options' => [
            'placeholder' => 'Select a state ...',
            'data-target'=>'#car_model',
            'onchange'=>'getCarModels(this)'
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'model_id')->widget(Select2::className(),  [
        'data' => [],
        'options' => [
            'placeholder' => 'Select a state ...',
            'id' => 'car_model'
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'drive_unit')->dropDownList(CarHelper::getDriveUnitDropdown(),['prompt'=> '_ _ _ _']) ?>

    <?= $form->field($model, 'engine_type')->dropDownList(CarHelper::getEngineTypeDropdown(),['prompt'=> '_ _ _ _']) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

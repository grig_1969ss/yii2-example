<?php

use app\common\helpers\CarHelper;
use app\modules\admin\models\CarModel;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\grid\GridView;
use \app\modules\admin\models\CarBrand;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\search\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Posts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Post', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute'   =>'brand_id',
                'value' => function ($model)
                {
                    return $model->brand->name;
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'brand_id',
                    'data' => CarBrand::getCarBrandDropdown(),
                    'options' => [
                        'placeholder' => 'Select states ...',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    // ... other params
                ]),
            ],
            [
                'attribute'   =>'model_id',
                'value' => function ($model)
                {
                    return $model->model->name;
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'model_id',
                    'data' => CarModel::getCarModelAllDropdown(),
                    'options' => [
                        'placeholder' => 'Select states ...',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    // ... other params
                ]),
            ]  ,
            [
                'attribute'   =>'drive_unit',
                'value' => function ($model)
                {
                    return CarHelper::getDriveUnitDisplayValue($model->drive_unit);
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'drive_unit',
                    'data' => CarHelper::getDriveUnitDropdown(),
                    'options' => [
                        'placeholder' => 'Select states ...',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    // ... other params
                ]),
            ]  ,
            [
                'attribute'   =>'engine_type',
                'value' => function ($model)
                {
                    return CarHelper::getEngineTypeDisplayValue($model->engine_type);
                },
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'engine_type',
                    'data' => CarHelper::getEngineTypeDropdown(),
                    'options' => [
                        'placeholder' => 'Select states ...',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    // ... other params
                ]),
            ]  ,

            //'description:text',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

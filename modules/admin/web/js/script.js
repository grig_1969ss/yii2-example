var AJAX_URL_BACK;
$( document ).ready(function() {
   console.log('ready!');
});


function getCarModels(e) {
        var target = $(e).data("target");
        var brand_id = $(e).val();

        if(brand_id){
                $.ajax({
                        url: AJAX_URL_BACK,
                        type: "POST",
                        data: {
                                action:   'getCarModels',
                                brand_id:brand_id
                        },
                        success: function (data) {
                                console.log(data);
                                if(data.data){
                                        $(target).val(null);
                                        $(target).html(data.data);
                                }else{
                                        alert('nooo');
                                }
                        },
                        error: function (error) {
                                console.log(error);
                                alert('error; ' + eval(error));
                        }
                });
        }else{
                $(target).html('');
        }
}

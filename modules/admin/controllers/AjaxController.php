<?php
/**
 * Created by PhpStorm.
 * User: Aram
 * Date: 1/23/2018
 * Time: 4:18 PM
 */

namespace app\modules\admin\controllers;

use app\modules\admin\models\CarModel;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\Response;

class AjaxController extends Controller
{

    public $enableCsrfValidation=false;

    public function beforeAction($action)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return parent::beforeAction($action);
    }

    public function actionCall(){
        $params = \Yii::$app->request->post();

        $action = ArrayHelper::getValue($params,'action');
        try{
            if(empty($action)){
                throw new Exception('No action');
            }
            $data = $this->$action($params);
            return [
                'respcode'=>1,
                'respmess'=>'Ok',
                'data'=> $data
            ];
        }catch (\Exception $e){
            return [
                'respcode'=>0,
                'respmess'=>$e->getMessage(),
				'data'=>$e->getTraceAsString()
            ];
        }
    }

    public function getCarModels($params){
        $brand_id = $params['brand_id'];

        $options = CarModel::getCarModelDropdown($brand_id);

        $html = '<option value="">_ _ _ _</option>';
        foreach ($options as $value=>$label){
            $html .='<option value="'.$value.'">'.$label.'</option>';
        }
        return $html;
    }
}
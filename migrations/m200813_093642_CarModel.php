<?php

use yii\db\Migration;

/**
 * Class m200813_093641_CarBrand
 */
class m200813_093642_CarModel extends Migration
{

    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('car_model', [
            'id' => $this->primaryKey()->notNull(),
            'name' => $this->string()->notNull()->comment('Модель'),
            'brand_id' => $this->integer()->notNull()->comment('Марка'),
            'created_at' => $this->dateTime()->notNull()->comment('Создано на'),
            'updated_at' => $this->dateTime()->notNull()->comment('Редактировано на'),
        ], $tableOptions);

        // creates index for column `brand_id`
        $this->createIndex(
            'car_model_fk1',
            'car_model',
            'brand_id'
        );

        // add foreign key for table `car_brand`
        $this->addForeignKey(
            'car_model_fk1',
            'car_model',
            'brand_id',
            'car_brand',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('car_model');
    }

}

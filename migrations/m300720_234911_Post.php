<?php

use yii\db\Migration;

class m300720_234911_Post extends Migration
{

    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('posts', [
            'id' => $this->primaryKey(),
            'brand_id' => $this->integer()->notNull()->comment('Марка'),
            'model_id' => $this->integer()->notNull()->comment('Модель'),
            'drive_unit' => $this->integer()->comment('Привод'),
            'engine_type' => $this->integer()->comment('Тип двигателя'),
            'description' => $this->text()->comment('Описание'),
            'created_at' => $this->dateTime()->comment('Создано на'),
            'updated_at' => $this->dateTime()->comment('Редактировано на'),
        ], $tableOptions);

        // creates index for column `brand_id`
        $this->createIndex(
            'posts_fk2',
            'posts',
            'brand_id'
        );

        // add foreign key for table `car_brand`
        $this->addForeignKey(
            'posts_fk2',
            'posts',
            'brand_id',
            'car_brand',
            'id',
            'CASCADE'
        );

        // creates index for column `model_id`
        $this->createIndex(
            'posts_fk3',
            'posts',
            'model_id'
        );

        // add foreign key for table `car_model`
        $this->addForeignKey(
            'posts_fk3',
            'posts',
            'model_id',
            'car_model',
            'id',
            'CASCADE'
        );

    }

    public function down()
    {
        $this->dropTable('posts');
    }

}
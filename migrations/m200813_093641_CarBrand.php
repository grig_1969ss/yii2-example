<?php

use yii\db\Migration;

/**
 * Class m200813_093641_CarBrand
 */
class m200813_093641_CarBrand extends Migration
{

    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('car_brand', [
            'id' => $this->primaryKey()->notNull(),
            'name' => $this->string()->notNull()->comment('Марка'),
            'created_at' => $this->dateTime()->notNull()->comment('Создано на'),
            'updated_at' => $this->dateTime()->notNull()->comment('Редактировано на'),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('car_brand');
    }

}
